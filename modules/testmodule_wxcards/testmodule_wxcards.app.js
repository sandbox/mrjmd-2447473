/**
 * User: gmalhotra
 * Date: 4/22/2014
 * Time: 13:11
 */
/* global twc */
/*jshint -W065 */

/* App Module */
twc.testmodule_wxcards = twc.testmodule_wxcards || {};
twc.testmodule_wxcards.app = twc.testmodule_wxcards.app || angular.module('testmodule_wxcards', []);
