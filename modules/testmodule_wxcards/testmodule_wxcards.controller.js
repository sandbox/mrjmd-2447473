/**
 * User: gmalhotra
 * Date: 4/22/2014
 * Time: 13:11
 */
/* global twc */
/*jshint -W065 */

twc.testmodule_wxcards.app.controller('twc_testmodule_wxcards_controller',['$scope', 'pcoUser', 'DrupalSettings', 'settings', 'dsxclient', 'twcConfig', 'customEvent', 'getRollingForecast', 'getFullForecast', function ($scope, pcoUser, DrupalSettings, settings, dsxclient, twcConfig, customEvent, getRollingForecast, getFullForecast) {
  'use strict';

  /**
   * Controller Variables
   */
  var status = twcConfig.module_status_codes;
  $scope.status = status.LOADING;
  $scope.showDetails = false;

  (function(){window.QueryString = function () {
      // This function is anonymous, is executed immediately and
      // the return value is assigned to QueryString!
      var query_string = {};
      var query = window.location.search.substring(1);
      var vars = query.split("&");
      for (var i=0;i<vars.length;i++) {
        var pair = vars[i].split("=");
        // If first entry with this name
        if (typeof query_string[pair[0]] === "undefined") {
          query_string[pair[0]] = pair[1];
          // If second entry with this name
        } else if (typeof query_string[pair[0]] === "string") {
          var arr = [ query_string[pair[0]], pair[1] ];
          query_string[pair[0]] = arr;
          // If third or later entry with this name
        } else {
          query_string[pair[0]].push(pair[1]);
        }
      }
      return query_string;
    };
  })();

  /**
   * Controller Functions
   */
  $scope.CCFunctions = {
    getData: function () {
      //Data from Drupal (DrupalSettings object)
      $scope.locId = TWC.PcoUtils.getURLlocid();

      dsxclient
        .execute([
          {$id: "datetime", recordType: "cs", recordName: "datetime", fullLocId: $scope.locId},
          {$id: "forecast", recordType: "xweb", recordName: "WebDFRecord", fullLocId: $scope.locId},
          {$id: "astro", recordType: "wxd", recordName: "Astro", date:"0", numOfDays:"10", fullLocId: $scope.locId},
          {$id: "obs", recordType: "wxd", recordName: "MORecord", fullLocId: $scope.locId},
          {$id: "nowcast", recordType: "wxd", recordName: "NCRecord", fullLocId: $scope.locId}
        ])
        .addResultsTo($scope)
        .then(function (response) {
          if (window.QueryString().testMode === 'true') {
            console.log(response);
            // Test data for forecast data (WebDFRecord)
            response.data.body[1].doc.WebDFData[0].wSpdM12 = 133;
            response.data.body[1].doc.WebDFData[0].wSpdK12 = 166;
            response.data.body[1].doc.WebDFData[0].wDirAsc12 = 'WNW';
            response.data.body[1].doc.WebDFData[0].uvIdx = 10;
            response.data.body[1].doc.WebDFData[0].precipTyp12 = 'snow';
            response.data.body[1].doc.WebDFData[0].pOP12 = 10;
            response.data.body[1].doc.WebDFData[0]._snwAccumTersePhrase12 = '1-3';
            response.data.body[1].doc.WebDFData[0]._snwAccumTersePhrase12_24 = '3-5';
            response.data.body[1].doc.WebDFData[0]._alt_snwAccumTersePhrase12 = '3-6';
            response.data.body[1].doc.WebDFData[0]._alt_snwAccumTersePhrase12_24 = '6-8';
            response.data.body[1].doc.WebDFData[1].sky12X = 3200;
            response.data.body[1].doc.WebDFData[0].sky12_24X = 300;
//            response.data.body[1].doc.WebDFData[0]._extPhrase12 = 'Run for the hills';
//            response.data.body[1].doc.WebDFData[0]._alt_extPhrase12 = 'Better go hide under a rock';
//            response.data.body[1].doc.WebDFData[0]._extPhrase12_24 = 'Chicken Little says, "The sky is falling"';
//            response.data.body[1].doc.WebDFData[0]._alt_extPhrase12_24 = 'Raining cats and dogs';
//            delete response.data.body[1].doc.WebDFData[3].narrative12;
//            delete response.data.body[1].doc.WebDFData[3].narrative24;
//
//            delete  response.data.body[1].doc.WebDFData[3].narrative12;

//            response.data.body[1].doc.WebDFData[3]._extPhrase12 = 'Run for the hills';


//            = 'English ipsum dolor sit amet, consectetur adipiscing elit. Nulla sit amet vulputate orci, ut viverra odio. Sed sed ornare nibh. Praesent eleifend non neque ac convallis. Sed vel sem libero. Proin sit amet neque interdum, pharetra quam ut, ornare libero amet.';
//            response.data.body[1].doc.WebDFData[0]._alt_narrative12 = 'Metric ipsum dolor sit amet, consectetur adipiscing elit. Nulla sit amet vulputate orci, ut viverra odio. Sed sed ornare nibh. Praesent eleifend non neque ac convallis. Sed vel sem libero. Proin sit amet neque interdum, pharetra quam ut, ornare libero amet.';
//            response.data.body[1].doc.WebDFData[0].narrative12_24 = 'English ipsum dolor sit amet, consectetur adipiscing elit. Nulla sit amet vulputate orci, ut viverra odio. Sed sed ornare nibh. Praesent eleifend non neque ac convallis. Sed vel sem libero. Proin sit amet neque interdum, pharetra quam ut, ornare libero amet.';
//            response.data.body[1].doc.WebDFData[0]._alt_narrative12_24 = 'Metric ipsum dolor sit amet, consectetur adipiscing elit. Nulla sit amet vulputate orci, ut viverra odio. Sed sed ornare nibh. Praesent eleifend non neque ac convallis. Sed vel sem libero. Proin sit amet neque interdum, pharetra quam ut, ornare libero amet.';
            response.data.body[1].doc.WebDFData[1].sky12X = 3200;
            response.data.body[1].doc.WebDFData[1].sky12_24X = 300;
            delete response.data.body[1].doc.WebDFData[1].narrative12;
            delete response.data.body[1].doc.WebDFData[1].narrative24;
            response.data.body[1].doc.WebDFData[1].narrative12_24 = "tedter";
//            response.data.body[1].doc.WebDFData[1].narrative24 = 'Narrative 24: Run for the hills';
            response.data.body[1].doc.WebDFData[1]._extPhrase12 = 'Run for the hills';
//            response.data.body[1].doc.WebDFData[1]._alt_extPhrase12 = 'Better go hide under a rock';
//            response.data.body[1].doc.WebDFData[1]._extPhrase12_24 = 'Chicken Little says, "The sky is falling"';
//            response.data.body[1].doc.WebDFData[1]._alt_extPhrase12_24 = 'Raining cats and dogs';

            // Enable Climatology Card
//            response.data.body[1].doc.WebDFData[0].locValDay = '20140618';
//            response.data.body[1].doc.WebDFData[0].locValTm = '235959';


            // Test data for obs data (MORecord)
            response.data.body[3].doc.MOData.tmpF = 100;
            // delete response.data.body[3].doc.MOData.tmpF;
            response.data.body[3].doc.MOData.tmpC = -20;
            response.data.body[3].doc.MOData.flsLkIdxF = 100;
            // delete response.data.body[3].doc.MOData.flsLkIdxF;
            response.data.body[3].doc.MOData.flsLkIdxC = -39;
            // response.data.body[3].doc.MOData.wx = "Isolated Thunderstrom and rain";
            response.data.body[3].doc.MOData.wx = "Sand/Dust Whirlwinds/Windy";
//            response.data.body[3].doc.MOData.sky = 35;

            // Wind
            response.data.body[3].doc.MOData.wDirAsc = 'WNW';
//            response.data.body[3].doc.MOData.wDirAsc = 'Variable';
            response.data.body[3].doc.MOData.wSpdM = 0;
            response.data.body[3].doc.MOData.wSpdK = 15;
            response.data.body[3].doc.MOData.wGstM = 20;
            response.data.body[3].doc.MOData.wGstK = 123;
            // delete response.data.body[3].doc.MOData.pres;
            // delete response.data.body[3].doc.MOData.alt;
//            response.data.body[3].doc.MOData.visM = -5;
//            response.data.body[3].doc.MOData.visK = -1;
            response.data.body[3].doc.MOData.uvIdx = 10;
            response.data.body[3].doc.MOData.iconExt = 300;
            response.data.body[3].doc.MOData.qulfrSvrty = 6;
            response.data.body[3].doc.MOData._extendedQulfrPhrase = "This is an extended obs qualifier. www www wasadwsfaw";

//            delete response.data.body[3].doc.MOData._extendedQulfrPhrase;

            // Test data for Nowcast data (NCRecord)
//            response.data.body[4].doc.NCData.NCpeakSvrty = 5;
            response.data.body[4].doc.NCData.NCphrase128 = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean mollis diam eros, ut porttitor odio vulputate eu. Sed cras amet.';
            response.data.body[4].doc.NCData.NCphrase256 = 'English ipsum dolor sit amet, consectetur adipiscing elit. Nulla sit amet vulputate orci, ut viverra odio. Sed sed ornare nibh. Praesent eleifend non neque ac convallis. Sed vel sem libero. Proin sit amet neque interdum, pharetra quam ut, ornare libero amet.';
            response.data.body[4].doc.NCData.NCmtrcPhrase = 'Metric ipsum dolor sit amet, consectetur adipiscing elit. Nulla sit amet vulputate orci, ut viverra odio. Sed sed ornare nibh. Praesent eleifend non neque ac convallis. Sed vel sem libero. Proin sit amet neque interdum, pharetra quam ut, ornare libero amet.';
//            response.data.body[3].doc.MOData.qulfrSvrty = 4;
//            response.data.body[3].doc.MOData._extendedQulfrPhrase = "This is an extended obs qualifier. www www wasadwsfaw";

          }


          if ($scope.forecast === null) {
            $scope.status = status.ERROR;
            return;
          }
          $scope.status = status.DEFAULT;

          $scope.CCFunctions.initForecast();
        })
        ["catch"](function () {
        $scope.status = status.ERROR;
      });
    },
    initForecast: function () {
      // Helper factories from wxcard.factory.js
      getRollingForecast($scope);
      getFullForecast($scope);
    }

  };

  /**
   * Initialize
   */
  $scope.CCFunctions.getData();

}]);
