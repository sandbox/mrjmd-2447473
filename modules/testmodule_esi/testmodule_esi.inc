<?php
/*
 * @file testmodule_esi.inc
 *
 * Adds dynamic processing of data passed through the esi tag.
 */
function angularmods_ngpanes_testmodule_esi_transform($vars) {
  // Example change.
  $vars['more_data'] = 'Test for more data passed in esi transform!';
  return $vars;
}
