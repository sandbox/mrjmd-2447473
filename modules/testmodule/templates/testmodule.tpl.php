<?php
/**
 * @file testmodule.tpl.php
 * Example tpl.php file for testmodule.
 */
?>
<div>
  Testmodule template...
  <?php
  echo " interpreted ";
    if (isset($variables['more_data'])) {
      echo $variables['more_data'];
    }
  ?>
</div>
