<?php
/**
 * @file testmodule.inc
 * Example helper function to transform data processed via ESI.
 * Adds dynamic processing of data passed through the esi tag.
 */
function angularmods_ngpanes_testmodule_transform($vars) {
  // Example change.
  $vars['more_data'] = 'Test for more data passed!';
  return $vars;
}
