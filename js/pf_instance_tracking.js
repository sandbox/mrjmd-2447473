// Provides UI helpers for angular instance tracking functionality.
jQuery(document).ready(function($){
    // Outline and Scroll to pane to be managed.
    var url = window.location;
    if (url.search !== '') {
        var urlParams = url.search.split('=');
        var pane_id = urlParams[1];
        var pane_div = $('#panel-pane-' + pane_id);
        pane_div.addClass('outline-instance');

        $('html, body').animate({
            scrollTop: pane_div.offset().top
        }, 2000);
    }
});
