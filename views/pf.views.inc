<?php

/**
 * @file
 * Presentation Framework Instance views integration.
 */

/**
 * Implements hook_views_data().
 */
function pf_views_data() {

  $data['pf_instances']['table']['group'] = t('PF Instance');

  $data['pf_instances']['table']['base'] = array(
    'field' => 'id',
    'title' => t('PF Unique ID'),
    'help' => t('PF Instances per Display'),
    'weight' => -10,
  );

  // Node ID table field.
  $data['pf_instances']['id'] = array(
    'title' => t('Instance Primary Key'),
    'help' => t('Primary Key.'),
  );

  // Dynamic link to the url Field.
  $data['pf_instances']['url'] = array(
    'title' => t('Edit Link'),
    'help' => t('The location of the edit page for that url.'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
  );

  // Pfmod field.
  $data['pf_instances']['pfmod'] = array(
    'title' => t('Pfmod name'),
    'help' => t('Machine Name of the pfmod being tracked.'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_string',
    ),
  );

  // Instance UUID field.
  $data['pf_instances']['pane_uuid'] = array(
    'title' => t('Instance pane UUID'),
    'help' => t('UUID of pfmod instance.'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_string',
    ),
  );

  // Display UUID field.
  $data['pf_instances']['display_uuid'] = array(
    'title' => t('Display UUID'),
    'help' => t('UUID of pfmod instances display.'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_string',
    ),
  );

  // Version field.
  $data['pf_instances']['version'] = array(
    'title' => t('Version Number'),
    'help' => t('Version of pfmod instance.'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_string',
    ),
  );

  return $data;
}
