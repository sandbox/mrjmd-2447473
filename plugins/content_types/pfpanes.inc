<?php

/**
 * @file
 * Presentation Framework Panes.
 *
 * Exposes PF modules per info files to panels for placement on the page.
 */

/**
 * Defines our parent plugin.
 */
$plugin = array(
  'title'        => t('Presentation Framework Module'),
  'description'  => t('Add and configure a PF module to the page'),
  'category'     => array(t('PF Modules'), -9),
  'content type' => 'pf_pfpanes_content_type_content_type',
  'admin info'   => 'pf_pfpanes_content_type_admin_info',
  'defaults'     => array(),
);

/**
 * Generate the content type (pane) definitions.
 *
 * @param string $subtype
 *   The requested module type.
 *
 * @return mixed
 *   Metadata for the content type requested.
 */
function pf_pfpanes_content_type_content_type($subtype) {
  $types = pf_pfpanes_content_type_content_types();
  if (isset($types[$subtype])) {
    return $types[$subtype];
  }

  drupal_set_message(
    t(
      "The presentation framework module (:module) appears to be missing. Remove from this panel, or replace the missing code and clear caches.",
      array(
        ':module' => $subtype,
      )
    ),
    'warning',
    FALSE
  );

  return FALSE;
}

/**
 * Utility function to generate the pane configs.
 *
 * This function determines which panes are available for use on any
 * particular panel.
 *
 * @return array
 *   Collection of pane metadata
 */
function pf_pfpanes_content_type_content_types() {
  // No reason to generate repeatedly, so static cache results.
  $types = & drupal_static(__FUNCTION__, array());
  if (!empty($types)) {
    return $types;
  }

  // Get all of the module metadata from the PF path.
  $modules = _pf_get_metadata();

  // Loop through the found modules and create panes for each.
  foreach ($modules as $module_key => $module) {
    // Default to not process the item. (opt in).
    $process = FALSE;

    // Skip non "module" includes.
    switch ($module->type) {
      case 'module':
      case 'static':
        $process = TRUE;
        break;

      default:
        continue;
    }

    // Skip to the next module if this one is not to be processed.
    if (!$process) {
      continue;
    }

    // Create the pane config.
    $types[$module_key] = array(
      'category'    => !empty($module->category) ? $module->category : 'PF Modules',
      'icon'        => 'icon_field.png',
      'title'       => $module->readable_module_name,
      'description' => $module->description,
      'defaults'    => array(),
      'cache'       => TRUE,
    );

    // Loop through required contexts, if any.
    if (isset($module->context_required)) {
      foreach ($module->context_required as $context_req) {
        $context = $context_req->type;
        if (isset($context)) {
          // Add the context requirement to the pane metadata.
          $types[$module_key]['required context'][] = new ctools_context_required(
            t('Required context'),
            $context
          );
        }
      }
    }
  }

  return $types;
}

/**
 * Implements hook_content_type_admin_title().
 */
function pf_pfpanes_content_type_admin_title($subtype, $conf, $context) {
  $module = _pf_get_metadata($subtype);
  if (empty($module->readable_module_name)) {
    $title = "BROKEN/MISSING MODULE";
  }
  else {
    $title = $module->readable_module_name;
  }

  return t(
    'READABLENAME (MODULENAME Presentation Module)',
    array(
      'READABLENAME' => $title,
      'MODULENAME'   => $subtype,
    )
  );
}

/**
 * Implements hook_content_type_admin_title().
 */
function pf_pfpanes_content_type_admin_info($subtype, $conf, $context) {
  $module = _pf_get_metadata($subtype);
  $block  = new stdClass();

  $block->title   = $subtype;
  $block->content = "$subtype";

  if (empty($module->readable_module_name)) {
    $block->title   = "BROKEN/MISSING MODULE ($subtype)";
    $block->content = "BROKEN/MISSING MODULE";
    return $block;
  }

  if (!empty($module->admin_presentation)) {
    if (!empty($module->admin_presentation->template)) {
      $template_file = $module->uri . '/' . $module->admin_presentation->template;
      if (file_exists($template_file)) {
        $block->content = theme_render_template($template_file, $conf);
      }
      else {
        drupal_set_message(
          t(
            'Administrative template specified for !module, but does not exist: !template',
            array(
              '!module'   => $subtype,
              '!template' => $template_file,
            )
          ),
          'error'
        );
      }
    }
    else {
      $block->content = '<h5>Raw instance data</h5><br/><pre>' . print_r($conf, 1) . '</pre>';
    }
  }
  return $block;
}

/**
 * Implements hook_content_type_edit_form().
 */
function pf_pfpanes_content_type_edit_form($form, &$form_state) {

  // Instance configuration for the form is in form_state['conf'];
  $conf = $form_state['conf'];
  // Get all of the module metadata from the PF path.
  $module = _pf_get_metadata($form_state['subtype_name']);

  // Isolate the form fields for the specific subtype.
  $form_conf = $module->configuration->fields;

  // Isolate validation array.
  if (isset($module->configuration->validation)) {
    $form_validation = $module->configuration->validation;
    // Add validation function to form processing.
    if (isset($form_validation) && is_array($form_validation)) {
      $form['#validate'] = array_merge($form_validation, $form['#validate']);
    }
  }

  // Remove the title override fields.
  unset($form['override_title_markup']);
  unset($form['override_title']);
  unset($form['override_title_text']);

  // Add template selection field if there is more then one template.
  if (count($module->template) > 1) {
    $choices       = array();
    $choices[NULL] = 'Template Selected By Theme';
    foreach ($module->template as $template) {
      $choices[$template->machine_name] = $template->label;
    }

    $template_field = array(
      '#type'        => 'select',
      '#title'       => 'Please choose a template.',
      '#options'     => $choices,
      '#description' => 'Select a template from drop down to force that selection. Otherwise template is selected according to info file template theme property, if it exists. If theme property does not exist, the first template in the array is selected.',
    );
    // TODO: Changing the name of this breaks stuff, but it's the last reference
    // to angular in the modue. What to do?
    $form['angular_template'] = $template_field;
    if (!empty($conf['angular_template'])) {
      $form['angular_template']['#default_value'] = $conf['angular_template'];
    }
  }

  // Get fields FAPI code as associative array.
  $form_conf = json_decode(json_encode($form_conf), TRUE);
  // We have $form_conf which is the nested associative FAPI array from the
  // info file. And we have the $conf array which is the nested "values".
  // Add the config provided form fields to the edit form.
  // Add version as hidden field.
  $form['version'] = array(
    '#type'  => 'hidden',
    '#value' => isset($conf['version']) ? $conf['version'] : $module->version,
  );

  $form['module_subtype'] = array(
    '#type'  => 'hidden',
    '#value' => $form_state['subtype_name'],
  );

  // Add Instance UUID as hidden field.
  $form['instance_uuid'] = array(
    '#type'  => 'hidden',
    '#value' => isset($conf['instance_uuid']) ? $conf['instance_uuid'] : ctools_uuid_generate(),
  );

  if (isset($form_conf)) {
    $form += $form_conf;

    foreach ($form_conf as $key => $field) {
      if (isset($conf[$key])) {
        $form[$key]['#default_value'] = $conf[$key];
      }
      if ((substr($key, 0, 1) !== '#') && is_array($form[$key])) {
        _pf_pfpanes_form_api_set_defaults($form, $conf, array($key));
      }
    }
  }
  return $form;
}

/**
 * Recursive form defaults setter for the custom modules.
 *
 * @param array $form
 *   Form API array.
 * @param array $conf
 *   Instance settings array from panels.
 * @param array $parents
 *   Current depth indicator and path to current value.
 */
function _pf_pfpanes_form_api_set_defaults(&$form, $conf, $parents) {
  $lconf = drupal_array_get_nested_value($conf, $parents);
  $lform = drupal_array_get_nested_value($form, $parents);

  if (((is_array($lconf) && !empty($lconf)) || !is_null($lconf)) && !empty($lform)) {
    $lparents   = $parents;
    $lparents[] = '#default_value';
    drupal_array_set_nested_value($form, $lparents, $lconf);
    $lform['#default_value'] = $lconf;
  }

  foreach ($lform as $key => $field) {
    if ((substr($key, 0, 1) !== '#') && is_array($field)) {
      $temp   = $parents;
      $temp[] = $key;
      _pf_pfpanes_form_api_set_defaults($form, $conf, $temp);
    }
  }
}

/**
 * Implements hook_content_type_edit_form_submit().
 */
function pf_pfpanes_content_type_edit_form_submit(&$form, &$form_state) {
  $form_state['conf'] = $form_state['values'];

  // Clear instance render cache.
  $cid = 'pfmod:' . $form_state['values']['module_subtype'] . ':' . $form_state['values']['instance_uuid'];
  cache_clear_all($cid, 'cache_pf', TRUE);
}

/**
 * Implements hook_content_type_render().
 */
function pf_pfpanes_content_type_render($subtype, $instance_config, $args, $context) {
  // Allow custom modules to do initial work.
  module_invoke_all('pf_pre_render');

  // Allow custom modules to add base dependencies.
  // We use it in angular to declare the app and load its dependencies.
  $base_dependencies = array();
  foreach (module_implements('pf_load_base_dependencies') as $custom) {
    $function = $custom . '_pf_load_base_dependencies';
    $function($base_dependencies, $subtype);
  }

  $cacheable = TRUE;
  // If an instance configuration provides a UUID, use it. If not, we should
  // not cache this item because the uuid will be different each time.
  if (empty($instance_config['instance_uuid'])) {
    $cacheable = FALSE;
    $instance_id = ctools_uuid_generate();
  }
  else {
    $instance_id = $instance_config['instance_uuid'];
  }

  // Create the cache key to be used for this object. Note that we are relying
  // on code elsewhere to clear this cache on modification. The md5 against
  // context is because context can change independently of instance config/ID.
  // Need to be able to cache for all contexts of a specific config. We add the
  // logged in check to prevent cached admin links from appearing in frontend.
  // It also mitigates the difference between esi delivery when logged in vs
  // not.
  $cid = "pfmod:$subtype:$instance_id:";
  $cid .= md5(
    json_encode(
      array(
        user_is_logged_in(),
        $context,
        $instance_config,
      )
    )
  );

  // Load module specific config.
  $module = _pf_get_metadata($subtype);

  if ($cacheable === TRUE) {
    // Attempt to fetch the cached pane.
    $cached = cache_get($cid, 'cache_pf');
    if (variable_get('pf_render_cache', TRUE) && ($cached !== FALSE)) {
      if (!empty($base_dependencies['js'])) {
        // Merge in the base dependency js.
        $cached->data->content['#attached']['js'] = _pf_pfpanes_merge_js(
          $cached->data->content['#attached']['js'],
          $base_dependencies['js']
        );
      }
      return $cached->data;
    }
  }

  // Pull the list of "required" fields for the module so that we can validate.
  $required_fields = _pf_pfpanes_list_expected_variables($subtype);

  // Default params array to empty.
  $params = array();

  // Check for template choice.
  $template = NULL;
  // TODO: Changing the name of this breaks stuff, but it's the last reference
  // to angular in the modue. What to do?
  if (!empty($instance_config['angular_template'])) {
    $template = $instance_config['angular_template'];
  }
  else {
    // If a template was not explicitly chosen, use the first template.
    $template = $module->template[0]->machine_name;
  }

  $dependencies = array();
  foreach (module_implements('pf_context_dependencies') as $custom) {
    $function = $custom . '_pf_context_dependencies';
    $function($dependencies, $context, $module, $subtype);
  }

  // Add in required fields from pane config (if any).
  foreach ($required_fields as $field) {
    if (isset($instance_config[$field])) {
      $params[$field] = $instance_config[$field];
    }
    else {
      $required = TRUE;
      // If this is an instance setting (as opposed to a context var).
      if (isset($module->configuration->fields->{$field})) {
        // If the field has a setting for "required".
        if (isset($module->configuration->fields->{$field}->{"#required"})) {
          // And if that setting is FALSE, then don't log the error.
          if (strtolower($module->configuration->fields->{$field}->{"#required"}) != "true") {
            $required = FALSE;
          }
        }
        else {
          // Not all fields need #required.
          $required = FALSE;
        }
        // If required field is not present, send an alert.
        if ($required) {
          pf_show_error(
            'Missing required %field param for %subtype module.',
            array('%field' => $field, '%subtype' => $subtype)
          );
        }
      }
    }
  }

  $block = NULL;
  // Change rendering depending on presentation type: ESI, inline or Angular.
  // Note that because ESI has both a fragment and a delivery call, we must
  // differentiate between them. On delivery call, treat as inline.
  // The user_is_logged_in() call is to allow modules to appear inline so the
  // page does not appear broken during development.
  if (
    module_exists('pf_esi') &&
    !user_is_logged_in() &&
    isset($module->presentation) &&
    $module->presentation == 'esi' &&
    variable_get('pf_use_esi', FALSE)
  ) {
    $block = _pf_esi_generate_esi_block($module, $instance_id, $template, $params, $context);
  }
  else {
    $block = _pf_pfpanes_render_block($dependencies, $module, $instance_id, $template, $params, $context);
  }

  // Default ttl to 30 days.
  $ttl = variable_get('pf_default_pfmod_ttl', 30 * 24 * 60 * 60);

  // Parse TTL and add to params.
  if (!empty($module->ttl)) {
    $ttl = $module->ttl;
  }

  // Save to the cache bin (if caching is enabled).
  if (variable_get('pf_render_cache', TRUE) && ($cacheable === TRUE)) {
    cache_set($cid, $block, 'cache_pf', time() + $ttl);
  }

  // Merge in the base dependency js.
  if (!empty($base_dependencies['js'])) {
    $block->content['#attached']['js'] = _pf_pfpanes_merge_js(
      $block->content['#attached']['js'],
      $base_dependencies['js']
    );
  }

  return $block;
}

/************* Utility functions below *************/

/**
 * Merge js dependency arrays.
 *
 * Adds $js2 to $js1 and returns the merged array.
 *
 * @param array $js1
 *   First array.
 * @param array $js2
 *   Second array.
 *
 * @returns array
 *   The merged array.
 */
function _pf_pfpanes_merge_js($js1, $js2) {
  $ret = array();

  $x = 0;
  if (!empty($js1)) {
    foreach ($js1 as $key => $val) {
      // If this is a 'setting' or 'inline' it will be numeric.
      if (is_numeric($key)) {
        $ret[$x++] = $val;
      }
      else {
        $ret[$key] = $val;
      }
    }
  }
  if (!empty($js2)) {
    foreach ($js2 as $key => $val) {
      // If this is a 'setting' or 'inline' it will be numeric.
      if (is_numeric($key)) {
        $ret[$x++] = $val;
      }
      else {
        $ret[$key] = $val;
      }
    }
  }
  return $ret;
}

/**
 * Render the actual contents of the module.
 *
 * @param array &$dependencies
 *   Dependencies array to be modified.
 * @param object $module
 *   Module of interest.
 * @param string $instance_id
 *   The unique ID for the module we are working on.
 * @param string $template
 *   The template to be used for rendering this instance.
 * @param array $params
 *   Array of vars from instance configuration / context.
 * @param array $context
 *   Context array (if available).
 *
 * @return object
 *   Panels rendereable.
 */
function _pf_pfpanes_render_block(array &$dependencies, $module, $instance_id, $template, $params, $context) {
  if (isset($module->module_name)) {
    $subtype = $module->module_name;
  }
  else {
    $subtype = 'MISSING/BROKEN';
  }

  // If there is a transform callback, load the include and call it.
  // Make sure the instance has a template declared and the module has templates
  // defined.
  if (!empty($module->template) && isset($template)) {
    // Loop through possible templates.
    foreach ($module->template as $module_temp) {
      // Is there a match?
      if ($module_temp->machine_name == $template) {
        // Does the match have a transform callback?
        if (!empty($module_temp->transform_callback)) {
          // Check the file exists, load and call the transform function.
          $inc = $module->uri . '/' . $module_temp->transform_callback->file;
          if (is_file($inc)) {
            if (!include_once $inc) {
              pf_show_error('Transform callback include could not be loaded ' . $module->readable_module_name);
            }
            $func = $module_temp->transform_callback->callback;
            if (function_exists($func)) {
              $params = $func($params);
            }
            else {
              pf_show_error(
                'Missing transform callback function "%func"',
                array('%func' => $func)
              );
            }
          }
          else {
            pf_show_error(
              'Transform callback include does not exist for %module',
              array('%module' => $module->readable_module_name)
            );
          }
        }
      }
    }
  }

  $div_opener = theme('pf_block_render', array(
    'module' => $module,
    'subtype' => $subtype,
    'instance_id' => $instance_id,
  ));

  // Create block to hold output.
  $block        = new stdClass();
  $block->title = '';

  $template_meta = _pf_load_template($module, $template);
  if (empty($template_meta)) {
    pf_show_error(
      'Presentation Framework was unable to find a template for a module (:module)',
      array(':module' => $subtype)
    );

    $block->content = "<placeholder type='{$module->type}'><!-- " . print_r($params, 1) . " --></placeholder>";
  }
  else {
    if (!isset($template_meta->type)) {
      $template_meta->type = 'static';
    }

    // Set the default placeholder if template fails to render.
    $content = "<placeholder type='{$template_meta->type}'><!-- " . print_r($params, 1) . " --></placeholder>";

    // Make sure module has a template file and then load it.
    if (!empty($template_meta->file)) {
      $template_file = $module->uri . '/' . $template_meta->file;
      if (file_exists($template_file)) {
        switch ($template_meta->type) {
          case 'static':
            $content = file_get_contents($template_file);
            break;

          case 'tpl.php':
            // Build/filter parameters from context data (if available).
            if (is_array($context)) {
              foreach ($context as $context_obj) {
                // If there are required contexts.
                if (!empty($module->context_required)) {
                  // Cycle through the required contexts.
                  foreach ($module->context_required as $req_context) {
                    // If the type matches a required context, add it to params.
                    if (isset($context_obj->type) && $req_context->type == $context_obj->type) {
                      // Evaluate and assign context object rather than keyword.
                      $params[$req_context->var] = $context_obj;
                    }
                    else {
                      // Evaluate and assign context object rather than keyword.
                      $params[$req_context->type] = $context_obj;
                    }
                  }
                }
              }
            }
            $content = theme_render_template($template_file, $params);
            break;
        }
      }
    }

    // Finish building block with template.
    $block->content = $div_opener . $content . '</div>';
  }

  // Load the metadata for all modules.
  $modules_metadata = _pf_get_metadata();

  // Load the instance settings into the js payload for the page.
  _pf_load_config($dependencies, $module, $instance_id, $params);

  // Load the javascript and css dependencies for this module.
  _pf_load_asset_dependencies($dependencies, $module);

  // Get the list of modules dependent upon this one.
  $modules = _pf_get_dependent_module_list($module);

  // Loop through all active modules and add CSS/JS dependencies.
  foreach (array_keys($modules) as $module_name) {
    $depmodule = $modules_metadata[$module_name];
    // Load dependencies for this shared module.
    _pf_load_asset_dependencies($dependencies, $depmodule);
  }

  // Load all template dependencies.
  _pf_load_template_asset_dependencies($dependencies, $module, $template);

  $block->content = array(
    '#markup'   => $block->content,
    '#attached' => $dependencies,
  );
  return $block;
}

/**
 * Return the list of variables that the module expects to receive.
 *
 * @param string $subtype
 *   Module type of interest.
 *
 * @return array
 *   Array of var names
 */
function _pf_pfpanes_list_expected_variables($subtype) {
  $module = _pf_get_metadata($subtype);

  $params = array();

  if (!empty($module->configuration->fields)) {
    // Get all of the "field" variable names.
    foreach (array_keys((array) $module->configuration->fields) as $field) {
      // Items beginning with a hash symbol are properties, not fields.
      if (substr($field, 0, 1) != '#') {
        $params[] = $field;
      }
    }
  }

  // Add in all of the "context" variable names.
  if (isset($module->context_required) && is_array($module->context_required)) {
    foreach ($module->context_required as $req) {
      // If there is no var defined use type.
      if (!isset($req->var)) {
        $req->var = $req->type;
      }
      $params[] = $req->var;
    }
  }

  return $params;
}

/**
 * Load JS and CSS dependencies for this module.
 *
 * Note that function must be ESI and cache "aware" in that anything done here
 * must be collected in such a way that it can both be effectively cached and
 * packaged ('#attached') for ESI.
 *
 * @param array &$dependencies
 *   Dependencies array to be modified.
 * @param object $module
 *   Module object of interest.
 */
function _pf_load_asset_dependencies(array &$dependencies, $module) {
  // Load themename to use in checking for theme specific css.
  global $theme;

  // Load js targets for module.
  foreach (array('header', 'footer') as $target) {
    if (empty($module->add_js->{$target})) {
      continue;
    }
    foreach ($module->add_js->{$target} as $js) {
      // If object is empty, skip it.
      if (empty($js->src)) {
        continue;
      }

      // Load js file.
      $js_file = $module->uri . '/' . $js->src;

      $dependencies['js'][$js_file] = array(
        'scope'      => $target,
        'group'      => _pf_js_decode_group($js),
        'every_page' => FALSE,
        'weight'     => $js->weight,
        'preprocess' => TRUE,
      );
    }
  }

  // Allow custom modules to extend dependencies.
  foreach (module_implements('pf_load_asset_dependencies') as $custom) {
    $function = $custom . '_pf_load_asset_dependencies';
    $function($dependencies, $module);
  }

  // Load css targets for module.
  foreach (array('header', 'footer') as $target) {
    if (empty($module->add_css->{$target})) {
      continue;
    }
    foreach ($module->add_css->{$target} as $css) {
      // Ensure a valid dependency exists.
      if (empty($module->uri) || empty($css->src)) {
        continue;
      }
      // Check for theme specific first.
      if (isset($css->themename)) {
        if ($css->themename == $theme) {
          $dependencies['css'][$module->uri . '/' . $css->src] = array(
            'scope'      => $target,
            'group'      => _pf_css_decode_group($css),
            'every_page' => FALSE,
            'weight'     => isset($css->weight) ? $css->weight : 0,
            'preprocess' => TRUE,
          );
        }
      }
      else {
        // Load anything else as general.
        $dependencies['css'][$module->uri . '/' . $css->src] = array(
          'scope'      => $target,
          'group'      => _pf_css_decode_group($css),
          'every_page' => FALSE,
          'weight'     => isset($css->weight) ? $css->weight : 0,
          'preprocess' => TRUE,
        );
      }
    }
  }
}

/**
 * Return a list of module dependencies for the specified module.
 *
 * This module attempts to collect a list of modules dependent upon the
 * currently being processed module. It uses recursion and a pass by ref arg
 * to prevent infinite loops.
 *
 * @param object $module
 *   Module object of interest.
 * @param array &$active_modules
 *   List of module dependencies as tracked recursively.
 *
 * @return array
 *   List of dependent modules to process. (see note above) This list may not
 *   be comprehensive based on the order of dependency requests.
 */
function _pf_get_dependent_module_list($module, &$active_modules = NULL) {

  // Initialize empty if first execution.
  if (is_null($active_modules)) {
    $active_modules = array();
  }

  // If we've already processed dependencies for this module, return empty.
  if (!empty($active_modules[$module->module_name])) {
    return array();
  }
  // Set this module as "processed".
  $active_modules[$module->module_name] = 1;

  // Pull meta data from cache.
  $modules_metadata = _pf_get_metadata();

  // Load shared targets recursively for module.
  if (isset($module->shared)) {
    foreach ($module->shared as $shared) {
      $shared_module = $modules_metadata[$shared->resource];

      // Check required module version against actual loaded version.
      if (isset($shared_module->version) && isset($shared->version)) {
        $actual   = explode('.', $shared_module->version);
        $required = explode('.', $shared->version);
        // Only check major versions.
        if ($actual[0] != $required[0]) {
          $message_values = array(
            '@module'  => $module->readable_module_name,
            '@version' => $shared->version,
            '@req'     => $shared_module->readable_module_name,
          );
          // Update user and log error.
          pf_show_error(
            'Presentation module required version does not match. Module "@module" expects version @version of required module  "@req".',
            $message_values
          );
        }
      }
      elseif (empty($shared_module)) {
        pf_show_error(
          'Required shared resource, "@req," specified in module, "@mod," does not exist.',
          array(
            '@req' => $shared->resource,
            '@mod' => $module->readable_module_name,
          )
        );
      }
      else {
        pf_show_error(
          'Required presentation module, "@req," does not have a version number.',
          array('@req' => $shared_module->readable_module_name)
        );
      }

      // Recurse and continue.
      _pf_get_dependent_module_list($shared_module, $active_modules);
    }
  }

  return $active_modules;
}

/**
 * Load JS and CSS dependencies for this template.
 *
 * @param array &$dependencies
 *   Dependencies array to be modified.
 * @param object $module
 *   Module object.
 * @param string $template
 *   Template object of interest.
 */
function _pf_load_template_asset_dependencies(array &$dependencies, $module, $template) {
  $template_meta = _pf_load_template($module, $template);
  // Refactor to get template assets only.
  if (isset($template_meta->assets)) {
    $template_assets = $template_meta->assets;
    // Load js targets for template.
    foreach (array('header', 'footer') as $target) {
      if (empty($template_assets->add_js->{$target})) {
        continue;
      }
      foreach ($template_assets->add_js->{$target} as $js) {
        $js_file = $module->uri . '/' . $js->src;
        $dependencies['js'][$js_file] = array(
          'scope'      => $target,
          'group'      => _pf_js_decode_group($js),
          'every_page' => FALSE,
          'weight'     => $js->weight,
          'preprocess' => TRUE,
        );
      }
    }

    // Load css targets for template.
    foreach (array('header', 'footer') as $target) {
      if (empty($template_assets->add_css->{$target})) {
        continue;
      }
      foreach ($template_assets->add_css->{$target} as $css) {
        $dependencies['css'][$module->uri . '/' . $css->src] = array(
          'scope'      => $target,
          'group'      => _pf_css_decode_group($css),
          'every_page' => FALSE,
          'weight'     => $css->weight,
          'preprocess' => TRUE,
        );
      }
    }
  }
}

/**
 * Load a template for a specified module.
 *
 * @param object $module
 *   Module object of interest.
 * @param string $machine_name
 *   Machine name for template.
 *
 * @return object
 *   Template metadata, or NULL if no template specified/found.
 */
function _pf_load_template($module, $machine_name = NULL) {
  // Load theme name to use in checking for theme specific templates.
  global $theme;
  $template = NULL;
  // If no template is specified, check to see if template specifies a theme.
  if (empty($machine_name)) {
    if (is_array($module->template)) {
      // Check to see if template is specified with a theme.
      foreach ($module->template as $template_data) {
        if ($template_data->theme == $theme) {
          $machine_name = $template_data->machine_name;
          break;
        }
      }
      // If machine name is still empty, use the first one listed in the module.
      if (empty($machine_name)) {
        $machine_name = $module->template[0]->machine_name;
      }
    }
    else {
      // In this situation, we may have a directive that will replace div
      // contents, so we simply show loading text.
      return NULL;
    }
  }

  // Pull out the template metadata.
  if (!empty($module->template)) {
    foreach ($module->template as $template_data) {
      if ($machine_name == $template_data->machine_name) {
        $template = $template_data;
      }
    }
  }

  return $template;
}

/**
 * Write module configuration to Drupal settings.
 *
 * Note that this function will add the settings to the Drupal.settings.*
 * namespace.
 *
 * @param array &$dependencies
 *   Dependencies array to be modified.
 * @param object $module
 *   Module class to load config.
 * @param string $instance_id
 *   Unique id for this instance.
 * @param array $params
 *   Collection of vars for use in the module.
 * @param bool $return_inline
 *   Return the inline JSON necessary to bootstrap the module.
 *
 * @return string|NULL
 *   String if $return_inline is TRUE
 */
function _pf_load_config(array &$dependencies, $module, $instance_id, $params, $return_inline = FALSE) {

  $module_name = $module->module_name;

  // Add the version to the instance data.
  $params['module_version'] = $module->version;

  // Build out the settings structure.
  $settings = array(
    'twc' => array(
      'instance' => array(
        $instance_id => $params,
      ),
      'modules'  => array(
        $module_name => array(
          $instance_id,
        ),
      ),
    ),
  );

  // TODO: Need to figure out what to do here, as this can't be in contrib.
  // Return inline is only called by pf_esi render, should it be a hook?
  if ($return_inline) {
    $json_instance = json_encode($settings['twc']['instance']);
    return "<script type='application/javascript'>" .
    "Drupal.settings.twc.instance['$instance_id'] = $json_instance;" .
    "Drupal.settings.twc.modules['$module_name'] = (Drupal.settings.twc.modules['$module_name'] || []).concat(['$instance_id']);" .
    "</script>";
  }

  // Add the instance settings to the js payload.
  $dependencies['js'][] = array(
    'data' => $settings,
    'type' => 'setting',
  );

  return NULL;
}
