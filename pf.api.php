<?php

/**
 * @file
 * Hooks provided by the Presentation Framework.
 */

/**
 * @addtogroup hooks
 * @{
 */

/**
 * Adds a listener to a directory for Presentation Framework modules.
 *
 * @return array
 *
 */
function hook_pf_get_dir() {
  return array(
    drupal_get_path('module', 'pf_example_angular') . '/modules',
  );
}

/**
 *
 */
function hook_pf_add_module(&$modules) {
  // Add the angular app to the modules array.
  $app_info_file_path = drupal_get_path('module', 'pf_example_angular') . '/app';
  $pf            = new stdClass();
  $pf->uri       = $app_info_file_path;
  $pf->filename  = 'pf.app';
  $pf->name      = 'pf';
  $modules['pf'] = $pf;
}

/**
 * Overrides default Presentation Framework module behaviors.
 *
 * @return array
 *   An array containing
 */
function hook_pf_mod_defaults() {
  return array(
    'type'                 => 'module',
    'presentation'         => 'static',
    'ttl'                  => '6000',
  );
}

/**
 *
 */
function hook_pf_js_groups() {
  return array(
    'default' => 100,
  );
}

/**
 *
 */
function hook_pf_css_groups() {
  return array(
    'default' => 100,
  );
}

/**
 * Performs global pre-render tasks for your modules.
 *
 * It has no return value, and should be used for initial work like
 * instantiating your framework of choice.
 */
function hook_pf_pre_render() {
  static $initialized;

  // No need to do this more than once.
  if (is_null($initialized)) {
    drupal_add_library('pf_example_angular', 'angularjs');
  }
}

/**
 *  Allows custom modules to add base dependencies.
 *
 *  We use it in angular example to declare the app and load any dependencies.
 */
function hook_pf_load_base_dependencies(&$dependencies, $subtype) {
  $module = _pf_get_metadata($subtype);
  // If this is a module, add to bootstrap list.
  if ($module->type == 'module') {
    $dependencies['js'][] = array(
      'data' => array(
        'pfmods' => array(
          'active' => array($module->module_name),
        ),
      ),
      'type' => 'setting',
    );
  }
  $is_processed[$module->module_name] = 1;
}

/**
 * Allows access to ctools context information.
 *
 * This is useful for providing contextual information to your framework.
 */
function hook_pf_context_dependencies($dependencies, $context, $module, $subtype) {
  // Build/filter parameters from context data (if available).
  if (is_array($context)) {
    $settings = array();
    foreach ($context as $context_obj) {
      // Only continue for required contexts.
      if (!empty($module->context_required)) {
        // Cycle through the required contexts.
        foreach ($module->context_required as $req_context) {
          // If the type matches a required context, only add the exposed
          // properties.
          $data = $context_obj->data;
          if (!empty($context_obj->keyword)) {
            // Place the context data in the D.settings.pf.contexts object.
            $settings['pf']['contexts'][$context_obj->keyword] = $data;
          }
        }
      }
    }
    if (!empty($settings)) {
      $dependencies['js'][] = array(
        'data' => $settings,
        'type' => 'setting',
      );
    }
  }
}

/**
 * Adds any module-specific dependencies.
 */
function hook_load_asset_dependencies($dependencies, $module) {
  // Embed relevant pfmod templates into the base page.
  if (!empty($module->template_cache)) {
    // Template_cache is an array and so loop through each of them.
    foreach ($module->template_cache as $template_file) {
      // Template file URL.
      $template_file = ('/' . $module->uri . '/' . $template_file->file);
      // If key is passed, use that as override else use file path as key.
      $template_key = isset($template_file->template_key) ? $template_file->key : $template_file;

      // Check if file exists before loading it.
      if (file_exists(DRUPAL_ROOT . $template_file)) {
        // Get file contents.
        $template_file_contents = file_get_contents(DRUPAL_ROOT . $template_file);
        // Create a custom script tag to contain the template.
        $element = array(
          '#type' => 'markup',
          '#markup' => $template_file_contents,
          '#prefix' => '<script id="' . $template_key . '" type="text/ng-template">',
          '#suffix' => '</script>',
        );
        $dependencies['drupal_add_html_head'][] = array($element, $template_key);
      }
    }
  }
}

/**
 * Custom theme callback for rendering pf blocks.
 */
function theme_pf_block_render($variables) {
  if (isset($variables['module']->type) && $variables['module']->type == 'module' && isset($variables['subtype'])) {
    $div_opener = '<div class="twc-init ' . $variables['subtype'] . '" data-twc-controller="twc_' . $variables['subtype'] . '_controller" instance="' . $variables['instance_id'] . '">';
  }
  else {
    $class_name   = 'my-custom-class';
    $div_opener   = '<div data-instance="' . $variables['instance_id'] . '" class="' . $class_name . '">';
  }
  return $div_opener;
}
