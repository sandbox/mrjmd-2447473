<?php

/**
 * Implements hook_angularmods_get_dir().
 *
 * TODO: This should be configurable.
 */
function pf_example_angular_pf_get_dir() {
  return array(
    drupal_get_path('module', 'pf_example_angular') . '/modules',
  );
}

/**
 * Implements hook_pf_js_groups().
 */
function pf_example_angular_pf_js_groups() {
  return array(
    'angular.general' => 7,
    'angular.shared' => 5,
    'library' => JS_LIBRARY,
    'default' => JS_DEFAULT,
  );
}

/**
 * Implements hook_pf_css_groups().
 */
function pf_example_angular_pf_css_groups() {
  return array(
    'angular.general' => 170,
    'angular.shared' => 150,
    'default' => CSS_DEFAULT,
  );
}

/**
 * Implements hook_pf_pre_render().
 *
 * Load base app dependencies.
 *
 * This function adds the PF base framework and dependencies. It uses a
 * static to ensure that it runs only once.
 */
function pf_example_angular_pf_pre_render() {
  static $initialized;

  // No need to do this more than once.
  if (is_null($initialized)) {
    $initialized = TRUE;

    drupal_add_library('pf_example_angular', 'angularjs');

    drupal_add_js('
        if (Drupal.settings.pf && Drupal.settings.pf.active){
          pf.app = pf.module("pf", Drupal.settings.pf.active);

        }
        window.page_bottom_reached = true;
      ',
      array(
        'type'   => 'inline',
        'scope'  => 'footer',
        'weight' => 17,
        'group'  => 10,
      )
    );

    drupal_add_js(
      drupal_get_path('module', 'pf_example_angular') . '/app/pf.app.js',
      array(
        'scope'      => 'header',
        'group'      => _pf_js_decode_group('angular.general'),
        'every_page' => FALSE,
        'weight'     => 12,
        'preprocess' => TRUE,
      )
    );
  }
}

/**
 * Implements hook_pf_add_module().
 */
function pf_example_angular_pf_add_module(&$modules) {
  // Add the angular app to the modules array for later return.
  $app_info_file_path = drupal_get_path('module', 'pf_example_angular') . '/app';
  $pf            = new stdClass();
  $pf->uri       = $app_info_file_path;
  $pf->filename  = 'pf.app';
  $pf->name      = 'pf';
  $modules['pf'] = $pf;
}

/**
 * Implements hook_library().
 */
function pf_example_angular_library() {
  $items = array();

  $module_path = drupal_get_path('module', 'pf_example_angular');

  $items['angularjs'] = array(
    'title'        => t('AngularJS'),
    'version'      => 'v1.2.3',
    'website'      => 'http://angularjs.org/',
    'js'           => array(
      $module_path . '/app/angular/angular.js'          => array(
        'type'   => 'file',
        'scope'  => 'footer',
        'group'  => JS_LIBRARY,
        'weight' => 10,
      ),
      $module_path . '/app/angular/angular-touch.js' => array(
        'type'   => 'file',
        'scope'  => 'footer',
        'group'  => JS_LIBRARY,
        'weight' => 11,
      ),
    ),
    'dependencies' => array(),
  );

  return $items;
}

/**
 * Implements hook_pf_load_base_dependencies().
 *
 * Load twc app dependencies (js, etc).
 */
function apf_example_angular_pf_load_base_dependencies(&$dependencies, $subtype = NULL) {
  static $initialized;

  // Do not do this more than once.
  if (is_null($initialized)) {
    $pf_module = _pf_get_metadata('pf.app');
    $initialized = TRUE;
    _pf_load_asset_dependencies($dependencies, $pf_module);

    // Get the list of modules dependent upon this one.
    $modules = _pf_get_dependent_module_list($pf_module);

    // Pull meta data from cache.
    $modules_metadata = _pf_get_metadata();

    // Loop through all active modules and add CSS/JS dependencies.
    foreach (array_keys($modules) as $module_name) {
      $module = $modules_metadata[$module_name];
      // Load dependencies for this shared module.
      _pf_load_asset_dependencies($dependencies, $module);
    }
  }

  if (!is_null($subtype)) {
    // Load module specific config.
    $module = _pf_get_metadata($subtype);
    static $is_processed;

    // Initialize our static (if necessary).
    if (is_null($is_processed)) {
      $is_processed = array();
    }

    // If we've already processed this module, don't add it to active again.
    // This array is used by the frontend to bootstrap angular, we want each
    // module to appear only once in this array.
    if (!isset($is_processed[$module->module_name])) {
      // If this is a module, add to bootstrap list.
      if ($module->type == 'module') {
        $dependencies['js'][] = array(
          'data' => array(
            'pfmods' => array(
              'active' => array($module->module_name),
            ),
          ),
          'type' => 'setting',
        );
      }
      $is_processed[$module->module_name] = 1;
    }
  }
}
