/** **/
pf.example_angular.app.controller('example_angular_controller',['$scope', 'settings', '$interpolate', 'PcoPage', '$rootScope', 'twcUtil', function ($scope, settings, $interpolate, PcoPage, $rootScope, twcUtil) {
	'use strict';
  // interpolation should be done by instance directive but for some reason if this happens first, interpolate it here.
  if(settings.title && settings.title.indexOf("{{") !== -1 && settings.title.indexOf("}}") !== -1) {
    $scope.title = $interpolate(settings.title)($rootScope);
  }
  else {
    $scope.title = settings.title;
  }
}]);