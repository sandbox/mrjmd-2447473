<?php
/**
 * @file example_static.inc
 * Example helper function to transform data.
 */
function pf_example_static_transform($vars) {
  // Example change.
  $vars['more_data'] = 'Test for more data passed!';
  return $vars;
}
