<?php
/**
 * @file testmodule.tpl.php
 * Example tpl.php file for testmodule.
 */
?>
<div>
  Testmodule template...
  <?php
  if (isset($variables['example_input'])) {
    echo 'Your custom text: ' . $variables['example_input'];
  }
  ?>
  <?php
  echo " interpreted ";
    if (isset($variables['more_data'])) {
      echo $variables['more_data'];
    }
  ?>
</div>
